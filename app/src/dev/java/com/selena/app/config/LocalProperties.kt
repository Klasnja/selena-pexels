package com.selena.app.config

class LocalProperties {

    val baseURL = "https://api.pexels.com/"

    val isLoggingEnabled = true

    val apiConnectTimeout: Long = 30
    val apiReadTimeout: Long = 30
    val apiWriteTimeout: Long = 30
}