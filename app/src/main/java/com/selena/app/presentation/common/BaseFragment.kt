package com.selena.app.presentation.common

import androidx.fragment.app.Fragment
import com.selena.app.util.EventBus
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.android.inject


@Suppress("UNCHECKED_CAST")
abstract class BaseFragment : Fragment() {
    val eventBus: EventBus by inject()

    val schedulers: Dispatchers by inject()

    val TAG: String = this::class.java.simpleName

    private fun baseActivity() = activity as BaseActivity
}