package com.selena.app.presentation.launch

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.ImagesData
import com.selena.app.domain.repositories.ImagesRepository
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.domain.utills.Dispatchers
import com.selena.app.presentation.common.BaseViewModel
import kotlinx.coroutines.launch
import retrofit2.Response

class ImagesDataViewModel(application: Application,
                          logRepository: LogRepository,
                          dispatchers: Dispatchers,
                          gson: Gson,
                          val imagesRepository: ImagesRepository)
    : BaseViewModel(application, logRepository, dispatchers, gson) {

    private val data = MutableLiveData<ApiModel<ImagesData>>()

    private var lastQuery : String = ""
    private var page  = 1


    fun retryFetching(){
        refresh(lastQuery)
    }

    fun getFreshData(query: String? = null) {
        page = 1
        refresh(query ?: lastQuery)
    }

    fun getNextPage() {
        page++
        refresh(lastQuery)
    }

    private fun refresh(query: String = "") {
        lastQuery = query
        tasksWhileInMemory.add(viewModelScope.launch(dispatchers.io()) {
            makeApiCall(data){
                getImagesData(query)
            }
        })
    }

    private suspend fun getImagesData(query: String): Response<ImagesData> {
        return imagesRepository.getImagesData(query, page)
    }

    fun getLiveEarthDataObservable() = data
}

