package com.selena.app.presentation.launch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.selena.app.R
import com.selena.app.data.api.models.ImagesData
import com.selena.app.data.api.models.Photo
import kotlinx.android.synthetic.main.item_photo.view.*

class PexelsDataAdapter() : RecyclerView.Adapter<PexelsDataAdapter.ViewHolder>() {

    private val data = ArrayList<Photo>()

    fun updateData(imagesData: ImagesData) {

        val freshPage = imagesData.page == 1
        if (freshPage) {
            data.clear()
        }
        val position = data.size
        this.data.addAll(imagesData.photos)
        if (freshPage) {
            notifyDataSetChanged()
        } else {
            notifyItemInserted(position)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: PexelsDataAdapter.ViewHolder, position: Int) {
        val photo = data[position]
        with(holder.itemView) {
            Glide
                    .with(img_photo)
                    .load(photo.src.tiny)
                    .centerCrop()
                    .placeholder(R.drawable.indeterminate_progressbar)
                    .into(img_photo)
            photo.photographer?.let {
                txt_phototgraph.text = it
            }
        }
    }
}