package com.selena.app.presentation.common

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.selena.app.data.api.models.ApiModel
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.domain.utills.Dispatchers
import com.selena.app.domain.utills.ExceptionUtil
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import retrofit2.Response

open class BaseViewModel(application: Application,
                         val logRepository: LogRepository,
                         val dispatchers: Dispatchers,
                         val gson: Gson ) : AndroidViewModel(application) {

    val TAG: String = this::class.java.simpleName

    protected val tasksWhileInMemory = mutableListOf<Job>()

    override fun onCleared() {
        tasksWhileInMemory.forEach { it.cancel() }
        super.onCleared()
    }

    suspend fun <T> makeApiCall(data: MutableLiveData<ApiModel<T>>, apiCall: suspend () -> Response<T>) {
        val responseData = makeApiCall(apiCall)
        data.postValue(responseData)
    }

    suspend fun <T> makeApiCall(apiCall: suspend () -> Response<T>): ApiModel<T> {
        return try {
            val response = apiCall.invoke()

            if (response.isSuccessful) {
                //basically handle what my Deferred type is holding
                ApiModel(model = response.body())
            } else {
                //Deal with non successful response.
                val exception = ExceptionUtil.mapApiException(gson, response.raw())
                logRepository.e("TAG", "Api Error", exception)
                ApiModel(throwable = exception)
            }

        } catch (e: Exception) {
            //Deal with error.

            val exception = ExceptionUtil.mapException(e)
            logRepository.e("TAG", "Api Error", exception)
            ApiModel(throwable = exception)
        }
    }
}