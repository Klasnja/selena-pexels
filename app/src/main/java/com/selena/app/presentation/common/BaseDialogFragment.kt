package com.selena.app.presentation.common


import androidx.appcompat.app.AppCompatDialogFragment
import com.selena.app.util.EventBus
import org.koin.android.ext.android.inject

@Suppress("UNCHECKED_CAST")
abstract class BaseDialogFragment : AppCompatDialogFragment() {

    val eventBus: EventBus by inject()

    val TAG: String = this::class.java.simpleName

    fun baseActivity() = activity as BaseActivity
}