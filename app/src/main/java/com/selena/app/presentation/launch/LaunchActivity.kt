package com.selena.app.presentation.launch

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.ImagesData
import com.selena.app.presentation.common.BaseActivity
import com.selena.app.presentation.components.ErrorComponent
import kotlinx.android.synthetic.main.activity_launch.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf
import com.selena.app.R
import android.content.Intent
import android.net.Uri


class LaunchActivity : BaseActivity() {

    private  val adapter = PexelsDataAdapter()

    val errorComponent: ErrorComponent by currentScope.inject { parametersOf(this) }

    val imagesDataViewModel: ImagesDataViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        setupRecyclerView()

        search_query.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newQuery: String): Boolean {

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                imagesDataViewModel.getFreshData(query)
                return false
            }

        })

        swiperefresh.setOnRefreshListener{
            imagesDataViewModel.getFreshData()
        }

        search_query.onActionViewExpanded()

        observeViewModel(imagesDataViewModel)
    }

    private fun observeViewModel(imagesDataViewModel: ImagesDataViewModel) {
        imagesDataViewModel.getLiveEarthDataObservable().observe(this, object : Observer<ApiModel<ImagesData>> {
            override fun onChanged(data: ApiModel<ImagesData>?) {
                swiperefresh.isRefreshing = false
                data?.model?.let {

                    if(it.totalResults > 0) {
                        setProminentLink()
                    } else {
                        setNoResultsFound()
                    }
                    updateData(it)
                }
                data?.throwable?.let{
                    errorComponent.handleError(it){
                        imagesDataViewModel.retryFetching()
                    }
                }
            }
        })
    }

    private fun setProminentLink() {
        val linkPlaceholder = "Pexels"
        val stringTerms = getString(R.string.prominent_link)
        val spannable = SpannableString(stringTerms)
        val indexTermsStart = stringTerms.indexOf(linkPlaceholder)
        val indexTermsEnd = indexTermsStart + linkPlaceholder.length

        spannable.setSpan(object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                ds.color = ds.linkColor
                ds.isUnderlineText = false;
            }
            override fun onClick(widget: View) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.pexels.com/"))
                startActivity(browserIntent)
            }
        }, indexTermsStart, indexTermsEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        txt_prominent_link.text = spannable
        txt_prominent_link.isClickable = true
        txt_prominent_link.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun setNoResultsFound(){
        txt_prominent_link.text = getString(R.string.no_photos)
    }


    private fun setupRecyclerView() {

        val layoutManager = FlexboxLayoutManager(this)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START

        rv_data.layoutManager = layoutManager
        rv_data.itemAnimator = DefaultItemAnimator()
        val itemDecor = DividerItemDecoration(this, RecyclerView.VERTICAL)
        rv_data.addItemDecoration(itemDecor)
        rv_data.adapter = adapter

        rv_data.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val manager = layoutManager as FlexboxLayoutManager
                val totalItemCount = manager.itemCount
                // enable pull to refresh only if the firstItem is on the top
                val firstVisibleItem = manager.findFirstCompletelyVisibleItemPosition()
                val isFirstItemOnTheTop = firstVisibleItem == 0 || firstVisibleItem == RecyclerView.NO_POSITION
                swiperefresh?.apply {
                    isEnabled = isFirstItemOnTheTop
                }
                val lastVisible = manager.findLastVisibleItemPosition()
                val endHasBeenReached = lastVisible + 7 >= totalItemCount
                if (totalItemCount > 0 && endHasBeenReached && newState== RecyclerView.SCROLL_STATE_DRAGGING) {
                    //you have reached to the bottom of your recycler view
                    imagesDataViewModel.getNextPage()
                }
            }
        })
    }

    private fun updateData(data: ImagesData) {
        adapter.updateData(data)
    }
}
