package com.selena.app.domain.repositories

import com.selena.app.data.api.models.ImagesData
import retrofit2.Response

interface ImagesRepository {

    suspend fun getImagesData(query: String="", page: Int = 1): Response<ImagesData>
}