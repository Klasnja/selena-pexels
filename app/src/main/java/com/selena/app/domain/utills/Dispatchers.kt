package com.selena.app.domain.utills

import kotlinx.coroutines.CoroutineDispatcher

open class Dispatchers {
    open fun ui(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.Main
    }

    open fun io(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.IO
    }

    open fun default(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.Default
    }

    open fun unconfirmed(): CoroutineDispatcher {
        return kotlinx.coroutines.Dispatchers.Unconfined
    }
}