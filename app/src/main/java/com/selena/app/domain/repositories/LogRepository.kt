package com.selena.app.domain.repositories

interface LogRepository {

    fun d(tag: String, message: String?)

    fun e(tag: String, message: String?)

    fun e(tag: String, message: String?, throwable: Throwable)

    fun v(tag: String, message: String?)
}