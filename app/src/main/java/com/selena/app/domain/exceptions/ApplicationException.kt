package com.selena.app.domain.exceptions


class ApplicationException(cause: Throwable,
                           val responseCode: Int? = null,
                           val title: Int? = null,
                           val bodyMessage: String? = null,
                           val messageResId: Int? = null) : BaseException(cause)