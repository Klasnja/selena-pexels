package com.selena.app.di

import com.selena.app.data.managers.AuthManager
import com.selena.app.data.managers.ImagesManager
import com.selena.app.data.managers.LogManager
import com.selena.app.di.NetworkModule.Companion.POST_INIT_PEXELS_API
import com.selena.app.domain.repositories.AuthRepository
import com.selena.app.di.NetworkModule.Companion.PRE_INIT_SERVICE_API
import com.selena.app.domain.repositories.ImagesRepository
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.domain.utills.Dispatchers
import com.selena.app.util.Logger
import com.selena.app.util.EventBus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module

@UseExperimental(ExperimentalCoroutinesApi::class)
val appModule = module {

    // region single
    single { AuthManager(get(named(PRE_INIT_SERVICE_API)), androidContext()) } bind AuthRepository::class

    single { ImagesManager(get(named(POST_INIT_PEXELS_API))) } bind ImagesRepository::class

    single { LogManager()} bind LogRepository::class

    single { Logger()}

    single { EventBus()}

    single { Dispatchers()}
    // end region
}