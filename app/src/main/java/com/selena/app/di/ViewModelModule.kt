package com.selena.app.di

import com.selena.app.presentation.launch.ImagesDataViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { ImagesDataViewModel(get(), get(), get(), get(), get()) }
}
