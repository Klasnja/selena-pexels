package com.selena.app.data.api.services
import com.selena.app.data.api.models.requests.LoginWithEmailRequest
import com.selena.app.data.api.models.responses.UserResponseData
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST


interface AuthApiService {

    @POST("/login_email")
    fun loginWithEmail(@Body request : LoginWithEmailRequest): Call<UserResponseData>
}
