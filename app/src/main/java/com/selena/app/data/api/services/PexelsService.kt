package com.selena.app.data.api.services

import com.selena.app.data.api.models.ImagesData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap


interface PexelsService {
    @GET("/v1/search")
    suspend fun getImageData(@QueryMap filters:  Map<String, String>): Response<ImagesData>

}

