package com.selena.app.data.api.models

import com.google.gson.annotations.SerializedName

data class ImagesData(@SerializedName("total_results") val totalResults: Long,
                      @SerializedName("page") val page: Int,
                      @SerializedName("per_page") val perPage: Int,
                      @SerializedName("photos") val photos: List<Photo> )
data class Photo(@SerializedName("id") val id: Long,
                 @SerializedName("url") val url: String,
                 @SerializedName("photographer") val photographer: String?,
                 @SerializedName("src") val src: Src)

data class Src(@SerializedName("original") val original: String,
               @SerializedName("tiny") val tiny: String)
