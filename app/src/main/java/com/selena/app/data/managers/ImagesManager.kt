package com.selena.app.data.managers

import com.selena.app.data.api.models.ImagesData
import com.selena.app.data.api.services.PexelsService
import com.selena.app.domain.repositories.ImagesRepository
import retrofit2.Response

class ImagesManager constructor(private val pexelsService: PexelsService) : ImagesRepository {

    override suspend fun getImagesData(query: String, page: Int): Response<ImagesData> {

        val filters = mutableMapOf<String, String>()
        filters["query"] = query
        filters["per_page"] = 30.toString()
        filters["page"] = page.toString()

        return pexelsService.getImageData(filters)
    }
}

