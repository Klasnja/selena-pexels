package com.selena.app.data.managers

import android.util.Log
import com.selena.app.domain.repositories.LogRepository

class LogManager : LogRepository {

    override fun d(tag: String, message: String?) {
        Log.d(tag, message)
    }

    override fun e(tag: String, message: String?) {
        Log.e(tag, message)
    }

    override fun e(tag: String, message: String?, throwable: Throwable) {
        Log.e(tag, message, throwable)
    }

    override fun v(tag: String, message: String?) {
        Log.v(tag, message)
    }
}